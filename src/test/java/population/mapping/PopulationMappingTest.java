package population.mapping;

import org.junit.Test;
import population.mapping.PopulationMapping.Point;
import population.mapping.PopulationMapping.Zone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class PopulationMappingTest {

    /**
     * xx
     * xx
     */
    @Test
    public void testZoneSplit_SplitableSquare() {
        Point topLeft = new Point(0, 0);
        Point botRight = new Point(1, 1);
        Zone zone = new Zone(topLeft, botRight);
        zone.doSplit();
        assertEquals(new Zone(new Point(0, 0), new Point(0, 0)), zone.getNw());
        assertEquals(new Zone(new Point(1, 0), new Point(1, 0)), zone.getNe());
        assertEquals(new Zone(new Point(0, 1), new Point(0, 1)), zone.getSw());
        assertEquals(new Zone(new Point(1, 1), new Point(1, 1)), zone.getSe());
    }

    /**
     * xx
     * xx
     * xx
     */
    @Test
    public void testZoneSplit_SplitableRectangle() {
        Point topLeft = new Point(0, 0);
        Point botRight = new Point(1, 2);
        Zone zone = new Zone(topLeft, botRight);
        zone.doSplit();
        assertEquals(new Zone(new Point(0, 0), new Point(0, 1)), zone.getNw());
        assertEquals(new Zone(new Point(1, 0), new Point(1, 1)), zone.getNe());
        assertEquals(new Zone(new Point(0, 2), new Point(0, 2)), zone.getSw());
        assertEquals(new Zone(new Point(1, 2), new Point(1, 2)), zone.getSe());
    }

    /**
     * xxx
     * xxx
     */
    @Test
    public void testZoneSplit_SplitableRetangle2() {
        Point topLeft = new Point(0, 0);
        Point botRight = new Point(2, 1);
        Zone zone = new Zone(topLeft, botRight);
        zone.doSplit();
        assertEquals(new Zone(new Point(0, 0), new Point(1, 0)), zone.getNw());
        assertEquals(new Zone(new Point(2, 0), new Point(2, 0)), zone.getNe());
        assertEquals(new Zone(new Point(0, 1), new Point(1, 1)), zone.getSw());
        assertEquals(new Zone(new Point(2, 1), new Point(2, 1)), zone.getSe());
    }

    /**
     * xx
     */
    @Test
    public void testZoneSplit_NoSplitableVertical() {
        Point topLeft = new Point(0, 0);
        Point botRight = new Point(1, 0);
        Zone zone = new Zone(topLeft, botRight);
        assertFalse(zone.doSplit());
    }


    /**
     * xx
     */
    @Test
    public void testZoneSplit_NoSplitableHorizontal() {
        Point topLeft = new Point(0, 0);
        Point botRight = new Point(0, 1);
        Zone zone = new Zone(topLeft, botRight);
        assertFalse(zone.doSplit());
    }


}