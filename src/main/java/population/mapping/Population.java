package population.mapping;

public class Population {
    public static PopulationStat populationStat = new PopulationStat() {
        @Override
        public int queryRegion(int x1, int y1, int x2, int y2) {
            return 0;
        }
    };

    public static int queryRegion(int x1, int y1, int x2, int y2) {
        return populationStat.queryRegion(x1, y1, x2, y2);
    }
}
