package population.mapping;

import java.util.*;
import java.util.stream.Collectors;

public class PopulationMapping {

    private static final char OCEAN = '.';
    private static final char LAND = 'X';
    public static final double COST_COEFFICIENT = 0.996;

    public String[] mapPopulation(int maxPercentage, String[] map, int totalPopulation) {
        World world = new World(totalPopulation * maxPercentage / 100, map, totalPopulation);
        Evaluation evaluation = new Evaluation(world);
        int count = 11;
        while (evaluation.hasNext() && count > 0) {
            Evaluation next = evaluation.next();
            System.out.println("total : " + world.totalWeight + " actual : " + next.result.population + " lands : " + next.result.landsTaken);
            evaluation = next;
            count--;
        }
        return evaluation.result.toResult();
    }

    private boolean shouldContinue(World world, Evaluation evaluation) {
        return (world.totalWeight - evaluation.result.population) / (double) world.totalWeight > 1 - COST_COEFFICIENT;
    }

    private static class World {
        private int totalWeight;
        private int totalPopulation;
        private String[] landMap;
        private boolean[][] landFlag;
        private int height;
        private int width;

        public World(int totalWeight, String[] landMap, int totalPopulation) {
            this.totalWeight = totalWeight;
            this.landMap = landMap;
            this.width = landMap[0].length();
            this.height = landMap.length;
            this.totalPopulation = totalPopulation;
            landFlag = toLandFlag(landMap);
        }

        private boolean[][] toLandFlag(String[] map) {
            int x = map[0].length();
            int y = map.length;
            boolean[][] isLand = new boolean[y][x];
            for (int i = 0; i < y; i++) {
                for (int j = 0; j < x; j++) {
                    isLand[i][j] = LAND == map[i].charAt(j);
                }
            }
            return isLand;
        }
    }

    private static class Result {
        private int landsTaken;
        private char[][] takenLandsMap;
        private int population;

        public Result(int landsTaken, int population, char[][] takenLandsMap) {
            this.landsTaken = landsTaken;
            this.takenLandsMap = takenLandsMap;
            this.population = population;
        }

        private String[] toResult() {
            String[] result = new String[takenLandsMap.length];
            for (int i = 0; i < takenLandsMap.length; i++) {
                result[i] = new String(takenLandsMap[i]);
            }
            return result;
        }

    }

    private static class Evaluation implements Iterator<Evaluation> {
        private World world;
        private Result result;
        private Queue<Zone> zonesQueue;

        public Evaluation(World world, Result result, Queue<Zone> zonesQueue) {
            this.world = world;
            this.result = result;
            this.zonesQueue = zonesQueue;
        }

        public Evaluation(World world) {
            this.world = world;
            this.result = new Result(0, 0, null);
            Zone root = new Zone(new Point(0, 0), new Point(world.width - 1, world.height - 1));
            root.pop = world.totalPopulation;
            root.countSelfLands(world.landFlag);
            this.zonesQueue = new PriorityQueue<>(Comparator
                    .<Zone>comparingInt(z -> z.getLands() * -1)
                    .thenComparingInt(z -> z.getPop()));
            zonesQueue.add(root);
        }

        @Override
        public boolean hasNext() {
            return !zonesQueue.isEmpty();
        }

        @Override
        public Evaluation next() {
            Zone current = zonesQueue.remove();
            if (current.doSplit() && current.getLands() > 0) {
                current.countChildrenLands(world.landFlag);
                current.queryChildrenPop();
                zonesQueue.add(current.nw);
                zonesQueue.add(current.ne);
                zonesQueue.add(current.sw);
                zonesQueue.add(current.se);
            }
            List<Zone> zones = zonesQueue.stream().collect(Collectors.toList());
            boolean[] taken = takeZones(world.totalWeight, zones, 10);
            Result currentResult = prepareResult(zones, taken);
            return new Evaluation(world, currentResult, zonesQueue);
        }

        private Result prepareResult(List<Zone> zones, boolean[] taken) {
            int takenLands = 0;
            int population = 0;
            char[][] takenLandsMap = copyLand(world.landMap, world.width, world.height);
            for (int i = 1; i < taken.length; i++) {
                if (!taken[i]) {
                    zones.get(i - 1).discardLand(takenLandsMap);
                } else {
                    takenLands += zones.get(i - 1).getLands();
                    population += zones.get(i - 1).getPop();
                }
            }
            return new Result(takenLands, population, takenLandsMap);
        }

        private boolean[] takeZones(int totalWeight, List<Zone> zones, int scaleFactor) {
            int[] weight = new int[zones.size() + 1];
            int[] profit = new int[zones.size() + 1];
            for (int i = 0; i < zones.size(); i++) {
                profit[i + 1] = zones.get(i).getLands();
                weight[i + 1] = zones.get(i).getPop();
            }
            int scaled = scaleToWeight(totalWeight, weight, scaleFactor);
            return knapsack01(zones.size(), scaled, profit, weight);
        }

        private char[][] copyLand(String[] map, int width, int height) {
            char[][] takenLands = new char[height][width];
            for (int i = 0; i < takenLands.length; i++) {
                takenLands[i] = Arrays.copyOf(map[i].toCharArray(), width);
            }
            return takenLands;
        }

        private int scaleToWeight(int weight, int[] weights, int scaleFactor) {
            if (weight < 1000000) return weight;
            int scale = weight / scaleFactor;
            for (int i = 1; i < weights.length; i++) {
                weights[i] = weights[i] / scaleFactor + 1;
            }
            return scale;
        }

        private static boolean[] knapsack01(int itemNumber, int totalWeight, int[] profit, int[] weight) {
            int[][] opt = new int[itemNumber + 1][totalWeight + 1];
            boolean[][] sol = new boolean[itemNumber + 1][totalWeight + 1];

            for (int n = 1; n <= itemNumber; n++) {
                for (int w = 1; w <= totalWeight; w++) {

                    // don't take item n
                    int option1 = opt[n - 1][w];

                    // take item n
                    int option2 = Integer.MIN_VALUE;
                    if (weight[n] <= w) option2 = profit[n] + opt[n - 1][w - weight[n]];

                    // select better of two options
                    opt[n][w] = Math.max(option1, option2);
                    sol[n][w] = (option2 > option1);
                }
            }

            // determine which items to take
            boolean[] take = new boolean[itemNumber + 1];
            for (int n = itemNumber, w = totalWeight; n > 0; n--) {
                if (sol[n][w]) {
                    take[n] = true;
                    w = w - weight[n];
                } else {
                    take[n] = false;
                }
            }
            return take;
        }

    }


    static class Zone {
        private Point topLeft;
        private Point botRight;
        private Zone nw;
        private Zone ne;
        private Zone sw;
        private Zone se;
        private int lands;
        private int pop;
        private boolean queried;
        private boolean counted;
        private final int area;

        public Zone(Point topLeft, Point botRight) {
            this.topLeft = topLeft;
            this.botRight = botRight;
            area = (botRight.x - topLeft.x) * (botRight.y - topLeft.y);
        }

        boolean doSplit() {
            if (botRight.x - topLeft.x < 1 || botRight.y - topLeft.y < 1) {
                return false;
            }
            Point center = topLeft.center(botRight);
            Point topCenter = topLeft.topCenter(botRight);
            Point botCenter = topLeft.botCenter(botRight);
            Point leftCenter = topLeft.leftCenter(botRight);
            Point rightCenter = topLeft.rightCenter(botRight);
            nw = new Zone(topLeft, center);
            ne = new Zone(topCenter.right(), rightCenter);
            sw = new Zone(leftCenter.down(), botCenter);
            se = new Zone(center.right().down(), botRight);
            return true;
        }

        public void querySelfPop() {
            if (!queried) {
                pop = Population.queryRegion(topLeft.x, topLeft.y, botRight.x, botRight.y);
                queried = true;
            }
        }

        public void queryChildrenPop() {
            ne.querySelfPop();
            nw.querySelfPop();
            se.querySelfPop();
            sw.pop = pop - ne.pop - nw.pop - se.pop;
        }

        private void countSelfLands(boolean[][] landMap) {
            if (!counted) {
                for (int i = topLeft.y; i <= botRight.y; i++) {
                    for (int j = topLeft.x; j <= botRight.x; j++) {
                        if (landMap[i][j]) {
                            ++lands;
                        }
                    }
                }
                counted = true;
            }
        }

        public void countChildrenLands(boolean[][] landFlag) {
            ne.countSelfLands(landFlag);
            nw.countSelfLands(landFlag);
            sw.countSelfLands(landFlag);
            se.lands = lands - ne.lands - nw.lands - sw.lands;
        }

        public Zone getNw() {
            return nw;
        }

        public Zone getNe() {
            return ne;
        }

        public Zone getSw() {
            return sw;
        }

        public Zone getSe() {
            return se;
        }

        public int getLands() {
            return lands;
        }

        public int getPop() {
            return pop;
        }


        @Override
        public String toString() {
            return "Zone{" +
                    "topLeft=" + topLeft +
                    ", botRight=" + botRight +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Zone zone = (Zone) o;

            if (botRight != null ? !botRight.equals(zone.botRight) : zone.botRight != null) return false;
            if (topLeft != null ? !topLeft.equals(zone.topLeft) : zone.topLeft != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = topLeft != null ? topLeft.hashCode() : 0;
            result = 31 * result + (botRight != null ? botRight.hashCode() : 0);
            return result;
        }

        public void discardLand(char[][] takenLands) {
            for (int i = topLeft.y; i <= botRight.y; i++) {
                for (int j = topLeft.x; j <= botRight.x; j++) {
                    takenLands[i][j] = OCEAN;
                }
            }
        }
    }


    static class Point {
        private int x;
        private int y;

        public Point(int x, int y) {
            if (x < 0 || y < 0)
                throw new IllegalArgumentException("Point coordination can't be negative x=" + x + " y=" + y);
            this.x = x;
            this.y = y;
        }

        public Point center(Point p) {
            return new Point(middle(this.x, p.x), middle(this.y, p.y));
        }

        public Point topCenter(Point p) {
            return new Point(middle(this.x, p.x), y);
        }

        public Point botCenter(Point p) {
            return new Point(middle(this.x, p.x), p.y);
        }

        public Point rightCenter(Point p) {
            return new Point(p.x, middle(this.y, p.y));
        }

        public Point leftCenter(Point p) {
            return new Point(x, middle(this.y, p.y));
        }


        public Point right() {
            return new Point(x + 1, y);
        }

        public Point down() {
            return new Point(x, y + 1);
        }

        private int middle(int a, int b) {
            return (a + b) / 2;
        }

        @Override
        public String toString() {
            return "Point{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Point point = (Point) o;

            if (x != point.x) return false;
            if (y != point.y) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }
    }
}
