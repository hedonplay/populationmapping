package population.mapping;

public interface PopulationStat {
    int queryRegion(int x1, int y1, int x2, int y2);
}
